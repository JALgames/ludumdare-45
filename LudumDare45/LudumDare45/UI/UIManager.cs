using System;
using System.Collections.Generic;
using LudumDare45.Game.World;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework.Input;

namespace LudumDare45.UI
{
    public static class UIManager
    {
        private static KeyboardState lastKeyboard, thisKeyboard;
        private static MouseState lastMouse, thisMouse;
        private static int scrollWheelStep = 120; // Set to a fixed 120 for now. If dynamic detection is wanted, set to int.MaxValue; // I'm sure this is 120 on most mice, but perhaps it isn't

        public static InventoryRenderer inventory;
        private static WorldSelector selector;
        private static List<UIComponent> components;
        public static void init()
        {
            lastKeyboard = thisKeyboard = new KeyboardState();
            lastMouse = thisMouse = new MouseState();

            components = new List<UIComponent>();
            
            components.Add(selector = new WorldSelector());
            components.Add(inventory = new InventoryRenderer(WorldManager.player.inventory));
        }

        public static void update()
        {
            updateInputs();

            foreach (var comp in components)
                comp.update();

            UIComponent.MouseEventTypes type = UIComponent.MouseEventTypes.Hover;
            
            if (lastMouse.LeftButton == ButtonState.Pressed && thisMouse.LeftButton == ButtonState.Released)
                type = UIComponent.MouseEventTypes.Release;
            if (lastMouse.LeftButton == ButtonState.Pressed && thisMouse.LeftButton == ButtonState.Pressed)
                type = UIComponent.MouseEventTypes.Drag;
            if (lastMouse.LeftButton == ButtonState.Released && thisMouse.LeftButton == ButtonState.Pressed)
                type = UIComponent.MouseEventTypes.Click;

            for (int i = components.Count - 1; i >= 0; i--)
                if (components[i].mouseEvent(thisMouse.X / PixelRenderer.pixelSize, thisMouse.Y / PixelRenderer.pixelSize, type))
                    break;
        }

        public static void draw()
        {
            foreach (var comp in components)
                comp.draw();
        }

        public static bool isKeyDown(Keys key)
        {
            return thisKeyboard.IsKeyDown(key);
        }
        public static bool isKeyDown(params Keys[] keys)
        {
            foreach (var key in keys)
                if (isKeyDown(key))
                    return true;

            return false;
        }

        public static int scrollDirection()
        {
            int delta = lastMouse.ScrollWheelValue - thisMouse.ScrollWheelValue;
            // Now that I think about it, this is a stupid idea: Laptops might report very small increments if they have
            // one of those fancy trackpads, but I still want the inventory to be usable on them
//            int abs = Math.Abs(delta);
//            if (abs > 0 && abs < scrollWheelStep)
//                scrollWheelStep = abs;

        return delta / scrollWheelStep;
        }
        
        private static void updateInputs()
        {
            lastKeyboard = thisKeyboard;
            thisKeyboard = Keyboard.GetState();

            lastMouse = thisMouse;
            thisMouse = Mouse.GetState();
        }
    }
}