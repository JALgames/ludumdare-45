namespace LudumDare45.UI
{
    public abstract class UIComponent
    {
        public abstract void update();
        public abstract void draw();
        public abstract bool mouseEvent(int x, int y, MouseEventTypes type);

        public enum MouseEventTypes
        {
            Hover,
            Click,
            Drag,
            Release
        }
    }
}