using System;
using LudumDare45.Game.World;
using LudumDare45.Game.World.Entities;
using LudumDare45.Game.World.Entities.PlayerStuff;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LudumDare45.UI
{
    public class InventoryRenderer : UIComponent
    {
        private GameTexture defaultTile;
        private GameTexture topEnd, bottomEnd;
        private GameTexture selectedMarker;
        
        private Inventory inventory;
        public int selected;
        public InventoryEntry selectedEntry => inventory.getEntry(selected);
        
        public InventoryRenderer(Inventory inventory)
        {
            this.inventory = inventory;
            Texture2D baseTexture = GameTexture.content.Load<Texture2D>(("ui/interface"));
            
            defaultTile = new GameTexture(baseTexture, new Rectangle(0, 0, 16, 16));
            topEnd = new GameTexture(baseTexture, new Rectangle(0, 15, 16, 1));
            bottomEnd = new GameTexture(baseTexture, new Rectangle(0, 0, 16, 1));
            
            selectedMarker = new GameTexture(baseTexture, new Rectangle(16, 0, 16, 16));
        }

        public override void update()
        {
            int newSelected = selected;
            if (UIManager.isKeyDown(Keys.D1))
                newSelected = 0;
            if (UIManager.isKeyDown(Keys.D2))
                newSelected = 1;
            if (UIManager.isKeyDown(Keys.D3))
                newSelected = 2;
            if (UIManager.isKeyDown(Keys.D4))
                newSelected = 3;
            if (UIManager.isKeyDown(Keys.D5))
                newSelected = 4;
            if (UIManager.isKeyDown(Keys.D6))
                newSelected = 5;
            if (UIManager.isKeyDown(Keys.D7))
                newSelected = 6;
            if (UIManager.isKeyDown(Keys.D8))
                newSelected = 7;
            if (UIManager.isKeyDown(Keys.D9))
                newSelected = 8;
            if (UIManager.isKeyDown(Keys.D0))
                newSelected = 9;

            if (newSelected >= inventory.slots)
                newSelected = selected; // We don't want those keys to do anything
            
            newSelected += UIManager.scrollDirection();
            while (newSelected < 0)
                newSelected += inventory.slots;
            newSelected %= inventory.slots;

            selected = newSelected;
        }

        public override void draw()
        {
            int n = inventory.slots;
            int startY = getStartY();
            int x = getX();
            
            PixelRenderer.draw(topEnd, x, startY - 1);
            PixelRenderer.draw(bottomEnd, x, startY + n * 16);
            for (int i = 0; i < n; i++)
            {
                PixelRenderer.draw(defaultTile, x, startY + 16 * i);
            }
            PixelRenderer.draw(selectedMarker, x, startY + 16 * selected);
        }

        private int getStartY()
        {
            int n = inventory.slots;

            int size = 16 * n;
            return (PixelRenderer.height - size) / 2;
        }

        private int getX()
        {
            return 0;
        }

        public override bool mouseEvent(int x, int y, MouseEventTypes type)
        {
            Rectangle position = new Rectangle(getX(), getStartY() - 1, 16, 2 + 16 * inventory.slots);
            if (!position.Contains(x, y))
                return false;
            if (type == MouseEventTypes.Click)
            {
                int relY = y - getStartY();
                if (relY > 0)
                {
                    int index = relY / 16;
                    if (index < inventory.slots)
                        selected = index;
                }
            }

            return true;
        }
    }
}