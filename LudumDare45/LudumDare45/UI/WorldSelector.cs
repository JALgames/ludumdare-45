using LudumDare45.Game.World;
using LudumDare45.Game.World.Entities.PlayerStuff;
using LudumDare45.Game.World.Items;
using LudumDare45.Game.World.Tiles;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LudumDare45.UI
{
    public class WorldSelector : UIComponent
    {
        private int targetX, targetY;
        private GameTexture[,] textures;
        private bool visible;
        public WorldSelector()
        {
            Texture2D texture = GameTexture.content.Load<Texture2D>("ui/interface");
            textures = new GameTexture[3, 3];
            
            int baseX = 32;
            int baseY = 0;
            for (int i = 0; i < 3; i++)
            {
                int x = (i == 0) ? 0 : i == 1 ? 5 : 11;
                x += baseX;
                int width = (i == 1) ? 6 : 5;
                for (int j = 0; j < 3; j++)
                {
                    int y = (j == 0) ? 0 : j == 1 ? 5 : 11;
                    y += baseY;
                    int height = (j == 1) ? 6 : 5;
                    textures[i, j] = new GameTexture(texture, new Rectangle(x, y, width, height));
                }
            }
        }
        
        public override void update()
        {
            visible = false;
        }

        public override void draw()
        {
            if (!visible)
                return;
            int startX = targetX * 16 - Camera.offsX;
            int startY = targetY * 16 - Camera.offsY;

            int growth = 2;
            for (int x = 0; x < 3; x++)
            {
                int offsX = (x == 0) ? -growth : x == 1 ? 5 : 11 + growth;
                for (int y = 0; y < 3; y++)
                {
                    int offsY = (y == 0) ? -growth : y == 1 ? 5 : 11 + growth;
                    PixelRenderer.draw(textures[x, y], offsX + startX, offsY + startY);
                }
            }
        }

        public override bool mouseEvent(int x, int y, MouseEventTypes type)
        {
            visible = true;
            targetX = (x + Camera.offsX) / 16;
            targetY = (y + Camera.offsY) / 16;
            if (type == MouseEventTypes.Click)
            {
                InventoryEntry invEntr = UIManager.inventory.selectedEntry;
                if (!invEntr.isEmpty)
                {
                    var result = ItemProvider.items[invEntr.itemIndex].primaryUse(targetX, targetY);
                    if (result.itemConsumed)
                        invEntr.count--;
                    
                }
            }
            return true;
        }
    }
}