using System;
using System.Collections.Generic;
using LudumDare45.Game.World.Tiles;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework;

namespace LudumDare45.Game.World
{
    public class TileLayer
    {
        public Tile[,] tiles;
        private int w => tiles.GetLength(0);
        private int h => tiles.GetLength(1);


        public TileLayer(int w, int h, Tile defaultTile)
        {
            this.tiles = new Tile[w, h];
            for (int x = 0; x < w; x++)
                for (int y = 0; y < h; y++)
                    tiles[x, y] = defaultTile;
        }

        private int rollingUpdate = 0;
        private int rollingUpdateSteps = 128;
        public void update()
        {
            for (int x = rollingUpdate; x < w; x += rollingUpdateSteps)
                for (int y = 0; y < h; y++)
                    tiles[x,y].update(x, y, rollingUpdateSteps);
            rollingUpdate++;
            if (rollingUpdate >= rollingUpdateSteps)
                rollingUpdate -= rollingUpdateSteps;
        }
        
        public void draw()        
        {
            int tolerance = 5;
            
            Rectangle screenRect = Camera.screenBounds;
            int minX = Math.Max(0, screenRect.X / 16 - tolerance);
            int minY = Math.Max(0, screenRect.Y / 16 - tolerance);
            int width = Math.Max(0, screenRect.Width / 16 + 2 * tolerance);
            int height = Math.Max(0, screenRect.Height / 16 + 2 * tolerance);
            
            for (int x = minX; x < minX + width; x++)
            for (int y = minY; y < minY + height; y++)
            {
                tiles[x, y].draw(x * 16 - screenRect.X, y * 16 - screenRect.Y);
            }
        }

        public List<Point> findEmpty(int x, int y, int w, int h)
        {
            List<Point> coords = new List<Point>();
            for (int i = Math.Max(0, x); i < Math.Min(this.w, x + w); i++)
            {
                for (int j = Math.Max(0, y); j < Math.Min(this.h, y + h); j++)
                {
                    if (tiles[i, j].GetType() == typeof(EmptyTile))
                        coords.Add(new Point(i, j));
                }
            }

            return coords;
        }

        public bool isEmpty(int x, int y)
        {
            return tiles[x, y].GetType() == typeof(EmptyTile);
        }
    }
}