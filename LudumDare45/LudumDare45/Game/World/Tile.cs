namespace LudumDare45.Game.World
{
    public abstract class Tile
    {
        public virtual void update(int x, int y, int t)
        {
            
        }
        public abstract void draw(int x, int y);
    }
}