using LudumDare45.Game.World.Entities.PlayerStuff;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework;

namespace LudumDare45.Game.World.Entities
{
    public abstract class Entity
    {
        public Vector2 center;
        public Vector2 size;
        public Vector2 topLeft => center - size / 2;

        public Inventory inventory;

        protected Point drawPosition => Camera.toPixelSpace(topLeft);

        public Entity(Vector2 center, Vector2 size)
        {
            this.center = center;
            this.size = size;
            this.inventory = new Inventory(8);
        }

        public abstract void update();
        public abstract void draw();
    }
}