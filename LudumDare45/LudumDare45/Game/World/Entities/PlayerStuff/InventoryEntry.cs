namespace LudumDare45.Game.World.Entities.PlayerStuff
{
    public class InventoryEntry
    {
        public bool isEmpty => itemIndex < 0 || count == 0;

        public int itemIndex;
        public int count;

        public InventoryEntry()
        {
            itemIndex = -1;
            count = 0;
        }
    }
}