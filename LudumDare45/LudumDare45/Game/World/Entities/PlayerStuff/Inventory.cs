using System;

namespace LudumDare45.Game.World.Entities.PlayerStuff
{
    public class Inventory
    {
        private InventoryEntry[] entries;
        public int slots => entries.Length;
        
        public Inventory(int size)
        {
            entries = new InventoryEntry[size];
            for (int i = 0; i < entries.Length; i++)
                entries[i] = new InventoryEntry();
        }
        
        public bool add(int itemIndex, int amount = 1)
        {
            // Try to find existing slot first
            for (int i = 0; i < entries.Length; i++)
            {
                if (entries[i].itemIndex == itemIndex && entries[i].count > 0)
                {
                    entries[i].count+= amount;
                    return true;
                }
            }
            
            // Otherwise create a new one
            for (int i = 0; i < entries.Length; i++)
            {
                if (entries[i].isEmpty)
                {
                    entries[i].count += amount;
                    entries[i].itemIndex = itemIndex;
                    return true;
                }
            }

            return false;
        }

        public bool has(int itemIndex, int amount = 1)
        {
            throw new NotImplementedException();
        }

        public bool take(int itemIndex, int amount = 1)
        {
            throw new NotImplementedException();
        }

        public InventoryEntry getEntry(int index)
        {
            return entries[index];
        }
    }
}