using LudumDare45.Game.World.Items;
using LudumDare45.Rendering;
using LudumDare45.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LudumDare45.Game.World.Entities
{
    public class Player : Entity
    {
        private GameTexture playerTexture;
        
        public Player(Vector2 position, Vector2 size) : base(position, size)
        {
            playerTexture = new GameTexture("player/player", new Rectangle(0, 0, 16, 16));

            inventory.add(ItemProvider.getIndex("grass-seed"));
            
            Camera.init(Camera.toPixelSpace(center));
        }
        
        public override void update()
        {
            float delta = 2 / 16f; // Two pixels per frame @ 60fps (hopefully I don't leave this like this and instead implement proper variable time step, but the game should hopefully run @ 60fps on most machines, so it's a good idea to optimize for that)

            Vector2 direction = new Vector2();
            if (UIManager.isKeyDown(Keys.Up, Keys.W))
                direction.Y -= 1;
            if (UIManager.isKeyDown(Keys.Down, Keys.S))
                direction.Y += 1;            
            
            if (UIManager.isKeyDown(Keys.Left, Keys.A))
                direction.X -= 1;
            if (UIManager.isKeyDown(Keys.Right, Keys.D))
                direction.X += 1;

            direction *= delta;
            center += direction;

            Camera.update(Camera.toPixelSpace(center));
        }        
        
        public override void draw()
        {
            PixelRenderer.draw(playerTexture, drawPosition - Camera.offset);
        }
    }
}