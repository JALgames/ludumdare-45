using System;
using System.Collections.Generic;
using System.Linq;
using LudumDare45.Game.World.Items.Nature;

namespace LudumDare45.Game.World.Items
{
    public static class ItemProvider
    {
        public static List<Item> items;
        private static Dictionary<string, int> indexLookup;
        
        public static void init()
        {
            items = new List<Item>();
            indexLookup = new Dictionary<string, int>();
            addItem("grass-seed", new GrassSeed());
        }

        private static void addItem(string name, Item item)
        {
            indexLookup.Add(name, items.Count);
            items.Add(item);
        }

        public static int getIndex(string name)
        {
            if (!indexLookup.ContainsKey(name))
                throw new Exception("Could not find item named " + name + ". :(");
            return indexLookup[name];
        }
        
    }
}