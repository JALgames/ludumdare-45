using LudumDare45.Rendering;

namespace LudumDare45.Game.World.Items
{
    public abstract class Item
    {
        public GameTexture texture;
        
        public Item(GameTexture texture)
        {
            this.texture = texture;
        }
        
        public abstract UseResult primaryUse(int x, int y);
        public abstract UseResult secondaryUse(int x, int y);

        public struct UseResult
        {
            public bool itemConsumed;
            public bool endClick;

            public UseResult(bool itemConsumed, bool endClick)
            {
                this.itemConsumed = itemConsumed;
                this.endClick = endClick;
            }

            public static readonly UseResult Nothing = new UseResult(false, false);
            public static readonly UseResult ConsumedContinue = new UseResult(true, false);
            public static readonly UseResult NotConsumedEndClick = new UseResult(false, true);
            public static readonly UseResult ConsumedEndClick = new UseResult(true, true);
        }
    }
}