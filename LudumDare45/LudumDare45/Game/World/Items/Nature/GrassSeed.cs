using LudumDare45.Game.World.Tiles;
using LudumDare45.Rendering;

namespace LudumDare45.Game.World.Items.Nature
{
    public class GrassSeed : Item
    {
        public GrassSeed() : base(SpriteSheetProvider.items.getTexture(0, 0))
        {
        }

        public override UseResult primaryUse(int x, int y)
        {
            if (WorldManager.groundGrowth.isEmpty(x, y))
            {
                WorldManager.groundGrowth.tiles[x, y] = new GrassTile();
                return UseResult.ConsumedContinue;
            }

            return UseResult.Nothing;
        }

        public override UseResult secondaryUse(int x, int y)
        {
            return UseResult.Nothing;
        }
    }
}