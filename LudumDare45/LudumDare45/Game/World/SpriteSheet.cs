using LudumDare45.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LudumDare45.Game.World
{
    public class SpriteSheet
    {
        private Texture2D texture;
        private int size;
        
        public SpriteSheet(string name, int size = 16)
        {
            texture = GameTexture.content.Load<Texture2D>(name);
            this.size = size;
        }
        
        public GameTexture getTexture(int x, int y, int w = 1, int h = 1)
        {
            return new GameTexture(texture, new Rectangle(x * size, y * size, w * size, h * size));
        }
    }
}