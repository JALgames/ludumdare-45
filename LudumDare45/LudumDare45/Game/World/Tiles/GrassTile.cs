using System.Collections.Generic;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework;

namespace LudumDare45.Game.World.Tiles
{
    public class GrassTile : Tile
    {
        public static List<GameTexture> textures;

        public int growthState;
        public bool[] drawnBlades;
        public int growthTimer;
        
        public GrassTile()
        {
            if (textures == null) // Lazy initialization, yay!
            {
                textures = new List<GameTexture>();
                for (int i = 0; i < 5; i++)
                    textures.Add(SpriteSheetProvider.floor.getTexture(i + 1, 0));
            }

            drawnBlades = new bool[textures.Count];
            addBlade();
        }

        private void addBlade()
        {
            if (growthState >= textures.Count)
                return;
            
            int index = Randomizer.rnd.Next(0, textures.Count - growthState);
            for (int i = 0; i < drawnBlades.Length; i++)
            {
                if (drawnBlades[i])
                    continue;
                if (index == 0)
                {
                    drawnBlades[i] = true;
                    break;
                }
                index--;
            }

            growthState++;
            int totalGrowTime = 120 * 60;
            int timePerStep = totalGrowTime / textures.Count;
            growthTimer = Randomizer.rnd.Next(timePerStep / 2, 3 * timePerStep / 2);
        }

        public override void update(int x, int y, int t)
        {
            growthTimer -= t;
            if (growthTimer <= 0)
                addBlade();

            int spawnTime = 90 * 60;
            if (Randomizer.rnd.Next(0, spawnTime) < t)
            {
                var empties = WorldManager.groundGrowth.findEmpty(x - 2, y - 2, 5, 5);
                if (empties.Count > 0)
                {
                    Point newGrass = empties[Randomizer.rnd.Next(0, empties.Count)];
                    WorldManager.groundGrowth.tiles[newGrass.X, newGrass.Y] = new GrassTile();
                }
            }
        }

        public override void draw(int x, int y)
        {
            for (int i = 0; i < drawnBlades.Length; i++)
                if (drawnBlades[i])
                    PixelRenderer.draw(textures[i], x, y);
        }
    }
}