using LudumDare45.Rendering;

namespace LudumDare45.Game.World.Tiles
{
    public abstract class SimpleFloorTile : Tile
    {
        protected abstract GameTexture getTexture();

        public override void draw(int x, int y)
        {
            PixelRenderer.draw(getTexture(), x, y);
        }
    }

    class DefaultFloorTile : SimpleFloorTile
    {
        protected override GameTexture getTexture()
        {
            return SpriteSheetProvider.floor.getTexture(0, 0);
        }
    }
}