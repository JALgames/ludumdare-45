using System.Collections.Generic;
using LudumDare45.Game.World.Entities;
using LudumDare45.Game.World.Items;
using LudumDare45.Game.World.Tiles;
using LudumDare45.Rendering;
using Microsoft.Xna.Framework;

namespace LudumDare45.Game.World
{
    public static class WorldManager
    {
        public static TileLayer ground;
        public static TileLayer groundGrowth;

        private static List<TileLayer> layers;

        public static List<Entity> entities;
        public static Player player;
        

        public static void init()
        {
            ItemProvider.init();
            
            layers = new List<TileLayer>();
            
            layers.Add(ground = new TileLayer(2048, 2048, new DefaultFloorTile()));
            layers.Add(groundGrowth = new TileLayer(2048, 2048, new EmptyTile()));
            
            player = new Player(new Vector2(1024, 1024), new Vector2(1, 1));
            
            entities = new List<Entity> {player};
        }


        public static void update()
        {
            foreach (var entity in entities)
                entity.update();
            foreach (var layer in layers)
                layer.update();
        }

        public static void draw()
        {
            foreach (var layer in layers)
                layer.draw();
            foreach (var entity in entities)
                entity.draw();
        }
    }
}