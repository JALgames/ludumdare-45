namespace LudumDare45.Game.World
{
    public static class SpriteSheetProvider
    {
        public static SpriteSheet floor;
        public static SpriteSheet items;

        public static void init()
        {
            floor = new SpriteSheet("tiles/floor");
            items = new SpriteSheet("player/items", 8);
        }
    }
}