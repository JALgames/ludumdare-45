using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LudumDare45.Rendering
{
    public static class PixelRenderer // Normally, I wouldn't make it static, but in this time-pressured environment, I don't think it will hurt that testing is more difficult
    {
        private static SpriteBatch sb;
        private static GraphicsDeviceManager graphics;
        
        public static int width, height;
        private static RenderTarget2D pixelCanvas;
        public static int pixelSize;
        
        public static void init(SpriteBatch sb, GraphicsDeviceManager graphics)
        {
            PixelRenderer.sb = sb;
            PixelRenderer.graphics = graphics;
            // Rest of init will be done during initial changeSize(...) call. I know, not very elegant...
        }

        public static void changeSize(int newWidth, int newHeight)
        {
            width = newWidth;
            height = newHeight;
            
            pixelCanvas?.Dispose();

            pixelCanvas = new RenderTarget2D(graphics.GraphicsDevice, width, height);
        }

        public static void startDraw()
        {
            graphics.GraphicsDevice.SetRenderTarget(pixelCanvas);
            graphics.GraphicsDevice.Clear(Color.Black);
            sb.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp);
        }

        public static void draw(GameTexture texture, Point p)
        {
            draw(texture, p.X, p.Y);
        }
        public static void draw(GameTexture texture, int x, int y)
        {
            sb.Draw(texture.texture, new Vector2(x, y), texture.sourceRect, Color.White);
        }

        public static void endDraw()
        {
            sb.End();
            graphics.GraphicsDevice.SetRenderTarget(null);
            sb.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp);
            sb.Draw(pixelCanvas,
                new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight),
                Color.White);
            sb.End();
        }
    }
}