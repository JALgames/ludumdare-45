using Microsoft.Xna.Framework;

namespace LudumDare45.Rendering
{
    public static class Camera
    {
        public static int centerX, centerY;

        public static Point offset => new Point(offsX, offsY);
        public static int offsX => centerX - PixelRenderer.width / 2;
        public static int offsY => centerY - PixelRenderer.height / 2;
        public static Rectangle screenBounds => new Rectangle(offsX, offsY, PixelRenderer.width, PixelRenderer.height);

        public static Point toPixelSpace(Vector2 worldSpace)
        {
            return new Point((int) (worldSpace.X * 16), (int) (worldSpace.Y * 16));
        }

        public static void init(Point target)
        {
            centerX = target.X;
            centerY = target.Y;
        }

        public static void update(Point target)
        {
            int maxX = 16;
            int maxY = 16;

            if (target.X - centerX > maxX)
                centerX = target.X - maxX;
            if (target.X - centerX < -maxX)
                centerX = target.X + maxX;
            
            if (target.Y - centerY > maxY)
                centerY = target.Y - maxY;
            if (target.Y - centerY < -maxY)
                centerY = target.Y + maxY;
        }
    }
}