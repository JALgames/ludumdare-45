using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LudumDare45.Rendering
{
    public class GameTexture
    {
        public static ContentManager content;
        
        public Texture2D texture;
        public Rectangle sourceRect;

        public GameTexture(string name)
        {
            texture = content.Load<Texture2D>(name);
            sourceRect = texture.Bounds;
        }  
        
        public GameTexture(string name, Rectangle sourceRect)
        {
            texture = content.Load<Texture2D>(name);
            this.sourceRect = sourceRect;
        }   
        
        public GameTexture(Texture2D texture, Rectangle sourceRect)
        {
            this.texture = texture;
            this.sourceRect = sourceRect;
        }
    }
}