﻿using System;
using LudumDare45.Game.World;
using LudumDare45.Rendering;
using LudumDare45.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LudumDare45
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.Window.AllowUserResizing = true;
            this.Window.ClientSizeChanged += new EventHandler<EventArgs>(Window_ClientSizeChanged);

            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            
            IsMouseVisible = true;
        }


        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            PixelRenderer.init(spriteBatch, graphics);
            adaptCanvasSize();
            GameTexture.content = Content;
            SpriteSheetProvider.init();
            
            WorldManager.init();
            UIManager.init();
        }
        protected override void Update(GameTime gameTime)
        {
            UIManager.update();
            WorldManager.update();
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            PixelRenderer.startDraw();

            WorldManager.draw();
            UIManager.draw();
            
            PixelRenderer.endDraw();
            base.Draw(gameTime);
        }
        
        private void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            if (graphics.PreferredBackBufferWidth != Window.ClientBounds.Width
                || graphics.PreferredBackBufferHeight != Window.ClientBounds.Height
                && Window.ClientBounds.Width != 0
                && Window.ClientBounds.Height != 0)
            {
                graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
                graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
                graphics.ApplyChanges();
                adaptCanvasSize();
            }
        }
        private void adaptCanvasSize()
        {
            int pixelSize = 5;
            if (graphics.PreferredBackBufferWidth > 1920 &&
                graphics.PreferredBackBufferHeight > 1080)
                pixelSize = 8;
            else
                pixelSize = 5;
            PixelRenderer.changeSize(graphics.PreferredBackBufferWidth / pixelSize,
                graphics.PreferredBackBufferHeight / pixelSize);
            PixelRenderer.pixelSize = pixelSize; // This is an unfortunate duplication of data, but I'm too lazy to refactor it
        }
    }
}
